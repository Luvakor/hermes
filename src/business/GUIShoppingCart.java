package business;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

public class GUIShoppingCart extends JFrame {

	private JPanel contentPane;
	private Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	private int w = screenSize.width;
	private int h = screenSize.height - 37;
	Client currentUser;


	/**
	 * Create the frame.
	 */
	public GUIShoppingCart(Client currentUser, ProductDatabase productDatabase) {
		
		if(currentUser == null)
			currentUser = new Client("Error", "No-pass", "noMail@noMail.com");
		else
			this.currentUser = currentUser;
		
		Color myColor;

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(0, 0, screenSize.width, screenSize.height);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		myColor = Color.decode("#34495e");
		contentPane.setBackground(myColor);
		contentPane.setLayout(null);
		
		//Container
		JPanel container = new JPanel();
		container.setPreferredSize(new Dimension(w, 2*h));
		myColor = Color.decode("#2c3e50");
		container.setLayout(null);
		container.setBackground(myColor);
		
		//Scroll
		JScrollPane scrollPanelContainer = new JScrollPane();
		scrollPanelContainer.setBounds(0, 0, w, h);
		scrollPanelContainer.setViewportView(container);
		
		contentPane.add(scrollPanelContainer);

		////Design/////
		JPanel yellowStripe = new JPanel();
		yellowStripe.setBounds(0, 3*h/22, w, h/22);
		myColor = Color.decode("#f1c40f");
		yellowStripe.setBackground(myColor);
		container.add(yellowStripe);
		
		JLayeredPane header = new JLayeredPane();
		header.setBounds(0, 0, 1350, 99);
		header.setBackground(new Color(0, 255, 255));
		container.add(header);
		myColor = Color.decode("#27ae60");
		
		JPanel logoPanel = new JPanel(); 
		myColor = Color.decode("#f39c12");
		logoPanel.setBackground(myColor);
		logoPanel.setBounds(0, 0, w/10, 3*h/22);
		header.add(logoPanel);
		
		JLabel logo = new JLabel();
		logo.setBounds(21, 0, 105, 105);
		ImageIcon i = new javax.swing.ImageIcon(getClass().getResource("/img/logo.png"));
		Image img = i.getImage();
		Image newImg = img.getScaledInstance(logo.getWidth(), logo.getHeight(), Image.SCALE_SMOOTH);
		ImageIcon image = new ImageIcon(newImg);
		logoPanel.setLayout(null);
		logo.setIcon(image);
		logoPanel.add(logo);
		
		JPanel nameLogoPanel = new JPanel();
		myColor = Color.decode("#f39c12");
		nameLogoPanel.setBackground(myColor);
		nameLogoPanel.setBounds(w/10, 0, w/8, 3*h/22);
		header.add(nameLogoPanel);
		nameLogoPanel.setLayout(null);
		
		JLabel name = new JLabel("HERMES");
		name.setForeground(new Color(0, 0, 0));
		name.setBounds(10, 29, 134, 45);
		name.setVerticalAlignment(SwingConstants.BOTTOM);
		name.setFont(new Font("Segoe UI", Font.BOLD, 33));
		nameLogoPanel.add(name);
		
		JPanel headerPanel = new JPanel();
		myColor = Color.decode("#1abc9c");
		headerPanel.setBackground(myColor);
		headerPanel.setBounds(0, 0, w, 3*h/22);
		header.add(headerPanel);
		headerPanel.setLayout(null);

		//User Card
		JPanel userPanel = new JPanel();
		myColor = Color.decode("#16a085");
		userPanel.setBackground(myColor);
		userPanel.setBounds(855, 10, w/3, 3*h/22);
		userPanel.setLayout(null);
		headerPanel.add(userPanel);
				
		JLabel userPhoto = new JLabel();
		userPhoto.setBounds(23, 17, 105, 105);
		ImageIcon userIcon = new javax.swing.ImageIcon(getClass().getResource("/img/defaultUser.png"));
						
		if(currentUser.userPhoto != null)
			userIcon = currentUser.userPhoto;
				
		Image imgUserPhoto = userIcon.getImage();
		Image newUserPhoto = imgUserPhoto.getScaledInstance(userPhoto.getWidth(), userPhoto.getHeight(), Image.SCALE_SMOOTH);
		userIcon = new ImageIcon(newUserPhoto);
		userPhoto.setIcon(userIcon);
		userPanel.add(userPhoto);
				
		JLabel lblLogUsername = new JLabel(currentUser.username);
		lblLogUsername.setFont(new Font("Bahnschrift", Font.BOLD, 23));
		lblLogUsername.setForeground(Color.WHITE);
		lblLogUsername.setBounds(150, 17, 140, 30);
		userPanel.add(lblLogUsername);
				
		JLabel lblLogEmail = new JLabel(currentUser.email);
		lblLogEmail.setFont(new Font("Bahnschrift", Font.ITALIC, 11));
		lblLogEmail.setForeground(Color.BLACK);
		lblLogEmail.setBounds(150, 44, 150, 20);
		userPanel.add(lblLogEmail);
				

		JButton logOutButton = new JButton("Log Out");
		logOutButton.setBounds(150, 63, 100, 18);
		logOutButton.setForeground(Color.BLACK);
		logOutButton.setFont(new Font("Bahnschrift", Font.BOLD, 13));
		myColor = Color.decode("#f1c40f");
		logOutButton.setBackground(myColor);
		userPanel.add(logOutButton);
		logOutButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				GUIHome guiHome = new GUIHome();
				guiHome.frame.setVisible(true);
				dispose();
			}
		});
				
		ImageIcon shp = new javax.swing.ImageIcon(getClass().getResource("/img/footH.png"));
		Image imgShp = shp.getImage();
		Image newShp = imgShp.getScaledInstance(37, 37, Image.SCALE_SMOOTH);
		shp = new ImageIcon(newShp);	
		JButton shpButton = new JButton(shp);
		shpButton.setBounds(320, 20, 50, 50);
		myColor = Color.decode("#f1c40f");
		shpButton.setBackground(myColor);
		userPanel.add(shpButton);
		final User auxUser = currentUser;
		final ProductDatabase fProductDatabase = productDatabase;
		shpButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				GUI_isLogged guiLogged = new GUI_isLogged(auxUser, fProductDatabase);
				guiLogged.setVisible(true);
				dispose();
			}
		});

		ImageIcon settings = new javax.swing.ImageIcon(getClass().getResource("/img/Settings.png"));
		Image imgSettings = settings.getImage();
		Image newSettings = imgSettings.getScaledInstance(37, 37, Image.SCALE_SMOOTH);
		settings = new ImageIcon(newSettings);
		JButton settingsButton = new JButton(settings);
		settingsButton.setBounds(390, 20, 50, 50);
		myColor = Color.decode("#f1c40f");
		settingsButton.setBackground(myColor);
		userPanel.add(settingsButton);
		logOutButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				GUIHome guiHome = new GUIHome();
				guiHome.frame.setVisible(true);
				dispose();
			}
		});	
		
		
		
	}

}
